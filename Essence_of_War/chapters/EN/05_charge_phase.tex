\section{CHARGE PHASE}
\EWquote{The melee swirled upon Korontav hill; The vermin's plagues spreading ill. Upon his hellish steed, the tyrant did shriek; Onwards to victory!}
{Excerpt from: The Account of the Battle of Korontav Moor, A Study on Dwarven Religion, by Cornelius Damstakle}

\begin{multicols}{2}
% Calcathin: I propose to comment this text below as a result of having added the paragraph before the multicols{2}
%\textit{This phase is often critical in defining the battle. Will you hold your troops back to see what your enemy does and try to counter their advances? Can you find an opportunity to attack opponents in their vulnerable flanks, or will you decide to charge headlong at the first opportunity? And how will your enemy react?}
The Charge Phase is when the Active Player has the chance to move their units into combat with enemy units. Declaring a Charge and then performing a successful Charge Move is usually the only way to engage an enemy unit in combat.%

%If you want any of your units to engage an enemy unit in Combat, you must declare which units will attempt to charge, and which enemy unit is the target, one at a time.%
Each time the Active Player declares a charge, the Reactive Player must declare the charged unit's Charge Reaction before moving to the next Charging Unit. You can use tokens to keep track of units' Charge Declarations and Reactions.%

\subsection{Round Sequence}
The Charge Phase is divided into the following steps:%

\begin{tabular}{c|p{0.38\textwidth}}
1 & Choose a unit and Declares a Charge\\
2 & Declare and resolve Charge Reaction\\
 & -- If the unit flees, all units charging this unit may Redirect the Charge\\
3 & Repeat steps 1--2 for all units that wish and can declare a Charge\\
4 & Choose a Charging unit, roll for Charge Range, and move the unit\\
5 & Repeat step 4 until Charging units have moved\\
\end{tabular}\\

\subsection{Declaring Charges}

Units that are Fleeing or that are Engaged in Combat cannot declare charges. Charges can only be declared at targets against which one or more models in the unit have Line of Sight to it and that the charging unit has a chance of a Successful Charge Move against. This means that there must be enough room to move the charger into base contact with its target, and that the target unit must be within the charger's potential Charge Range (\AdvanceRate{} + \distance{12}, see below).%

Friendly and enemy units cannot be moved into or through whilst Charging. When considering if a charge is possible, do not take any potential Flee Charge Reactions into account, but do take already declared charges into account, since other charging units might have a chance to move out of the way.%

\subsection{Charge Reactions}
Before declaring a Charge Reaction, determine in which Arc the unit will be charged.%

Each time the Active Player declares a charge, the Reactive Player must declare the charged unit's Charge Reaction before any further charges are declared. There are two different Charge Reactions: "Hold" and "Flee".%

\begin{itemize}[noitemsep,wide=0pt, leftmargin=\dimexpr\labelwidth + 2\labelsep\relax]
\item \textbf{Hold}: A Hold reaction means that the unit prepares for impact. A unit already Engaged in Combat can only choose a Hold reaction.%
\item \textbf{Flee}: The charged unit flees directly away from the charging enemy. Determine the direction by drawing a line from the Centre of the charging unit through the Centre of the charged unit and Pivot the fleeing unit so that the middle point of its Rear Facing is along this line. Then immediately make a Flee Move, by advancing \distance{\XDsix{2}} (see page \pageref{section:GeneralPrinciples_Flee}). An already fleeing unit that is charged must always choose to flee and will make an additional Flee Move.%
\end{itemize}

\subsubsection*{Redirecting a charge}
When a unit chooses the Flee Charge Reaction, the charger may try to Redirect the Charge by passing a \Discipline{} Test. If failed, the unit will try to complete the charge towards the unit that Fled. If passed, the unit can immediately declare a new charge towards another viable target unit, which chooses their Charge Reaction as normal. If more than one unit Declared a Charge against the Fleeing unit, each may try to Redirect its Charge in any order chosen by the Active Player.% 

A unit can only Redirect a Charge once per turn. If the situation arises that a unit Redirects a Charge and the second target also Flees, the charging unit may opt to charge either target, but must declare which one before rolling the Charge Range.%

\subsection{Move Chargers}
After all Charges and Charge Reactions have been declared and all Flee Moves completed, Charging Units will try to move into combat. Choose a unit that has declared a charge in this phase, roll its Charge Range and then perform the Charge Move. Repeat this with all units that have Declared a Charge this phase.%

\subsubsection*{Charge Range}
A unit's Charge Range is \XDsix{2} plus the unit's \AdvanceRate{} in inches, using the lowest \AdvanceRate{} among the unit. If this is equal to or higher than the distance between the charger and its intended target, the charger can proceed to make a Successful Charge Move. Measure the distance in a straight line from the closest point between the units.%

If the Charge Range is less than the distance or there is no space to complete the charge, the charge has failed and the charger performs a Failed Charge Move.%

\subsubsection*{Successful Charge Move}

A Successful Charge Move is resolved as follows:
\begin{itemize}[noitemsep,wide=0pt, leftmargin=\dimexpr\labelwidth + 2\labelsep\relax]
\item The unit may move forward an unlimited distance.%
\item A single \hyperref[section:MovementPhase_Wheels]{\textit{Wheel}} of up to 90 degrees may be performed during the move (see figure \ref{fig:EW_Charge}).%
\item The front of the charging unit must contact the Charged unit in the Facing determined when declaring the Charge Reaction.%
\item During a Charge Move units are allowed to come within \distance{1} of other units and \impassableterrain{} as per \hyperref[section:Armies_UnitSpacing]{\textit{Unit Spacing}} rule. It can only move into base contact with an enemy unit that it declared a Charge against, and it is also allowed to come into base contact with friendly units and \impassableterrain{}.%
\item Align units (see \hyperref[section:ChargePhase_AligningUnits]{\textit{Aligning Units}} below).%
\end{itemize}

\subsubsection*{Aligning Units}\label{section:ChargePhase_AligningUnits}
After the Charger manages to move into base contact with the Charged Unit, the units must now be aligned. An align move is performed as follows:%

\begin{enumerate}[noitemsep,wide=0pt, leftmargin=\dimexpr\labelwidth + 2\labelsep\relax]
\item Rotate the Charging Unit around the point where it contacted the unit, so that the Charging Unit's front and the Charged Unit's facing in which it was contacted are parallel.%
\item If this will not align the two units properly, for example due to interfering Terrain or other units, players may rotate the Charged Unit instead, or do a combination of the two, rotating the Charged unit as little as possible.%
\end{enumerate}

Align moves can only be made in the direction of alignment with the enemy units Charged Facing. Units can never be moved if they are already Engaged in Combat.%

\subsubsection*{Maximising Contact}\label{section:ChargePhase_MaximisingContact}
Charge moves must be made so the following conditions are satisfied as best as possible:

\begin{itemize}[noitemsep,wide=0pt, leftmargin=\dimexpr\labelwidth + 2\labelsep\relax]
\item 1st priority: The total number of units in the Combat are maximized. Note that this is only applicable when multiple units charge the same unit.%
\item 2nd priority: The number of models (on both sides) in base contact with at least one enemy model is maximised, including models Fighting Over Gaps (see page \pageref{section:FightingOverGaps}).%
\end{itemize}
As long as all above conditions are satisfied as best as possible, charging units are free to manoeuvre into position as they please while obeying the rules for Move Chargers.%

\end{multicols}

\begin{figure}[H]
\centering
\includegraphics[width=.95\textwidth]{diagrams/FIGURE EW Charge.png}
\caption{
	a) Unit A declares a charge at Unit B within its Line of Sight \& Charge Range. Unit B then declares Charge Reaction: Hold. Unit A rolls Charge Range (\XDsix{2}+\AdvanceRateInitials{}), and needs at least 8 to reach its target, which is the distance in inches from the closest points between the two units.%
	\\
	b) If Unit A rolled enough for its Charge Range, it now performs a Successful Charge Move. It wheels (once, less than 90 degrees) and moves forward until it reaches Unit B, \hyperref[section:ChargePhase_AligningUnits]{\textit{Aligning Units}} and  \hyperref[section:ChargePhase_MaximisingContact]{\textit{Maximizing Contact}}.%
	}
\label{fig:EW_Charge}
\end{figure}

\begin{multicols}{2}

%Calcathin
%\begin{figure}[H]
%\centering
%\includegraphics[width=0.45\textwidth]{diagrams/FIGURE EW Charge Frontage.png}
%\caption{
%	a) The majority of the frontage of the charging Unit B (light) is in the front arc of enemy Unit A (dark), so the charging unit must contact the front facing.%
%	\\
%	b) Unit B (light) moves forward and/or wheels into base contact following the Successful Charge Move rules. The \hyperref[section:ChargePhase_AligningUnits]{\textit{Aligning Units}} move is then performed by rotating the charging Unit B (light) around the point of contact.%
%	}
%\label{fig:EW_Charge_Frontage}
%\end{figure}

\subsubsection*{Combined Charges}
If more than one unit has declared a charge against a single enemy unit, chargers are moved in a slightly different manner. Roll the Charge Range for all the units charging that same target unit before moving any of them. Once it has been established which units will reach their target, the Active Player can move the Successful Charging units and/or the Failed Chargers in any order.%
\\

%Calcathin
%\begin{center}
%\includegraphics[width=.45\textwidth]{art/ORNAMENT_058_skull_stick.png}
%\end{center}

\subsubsection*{Engaged in Combat}\label{section:ChargePhase_EngagedInCombat}
As soon as a unit completes a Charge, it is Engaged in Combat (or Engaged) and will remain so as long as one or more models in the unit are in base contact with an enemy unit. If a unit is Engaged in Combat, all models in the unit are also considered to be Engaged in Combat. Units that are Engaged in Combat cannot move unless specifically stated otherwise, such as during \hyperref[section:MeleePhase_CombatPivot]{\textit{Combat Pivot}} or when \hyperref[section:MeleePhase_BreakTest]{\textit{Breaking from Combat}}).%

%Calcathin
%\begin{center}
%\includegraphics[width=.35\textwidth]{art/ORNAMENT_046_sword+map.png}
%\end{center}

\subsubsection*{Charging a Fleeing Unit}\label{section:ChargePhase_ChargingFleeingUnit}
When doing a Charge Move towards a Fleeing unit, follow the same rules as a normal Charge Move, except that the charging unit can move into contact with any Facing of its target, no aligning is made and no maximising of base to base contact is taken into consideration.%

Once the charger reaches base contact with the fleeing target, the entire fleeing unit is removed as a casualty. The charging unit may then perform a Pivot manoeuvre immediately afterwards by rotating the unit around its Centre (see page \pageref{section:MovementPhase_Pivots}).%

\subsubsection*{Impossible Charge}
When moving Charging Units, sometimes a situation occurs where units cannot reach combat. Units block each other from reaching combat, or there is not enough space to fit all chargers, etc. When this happens, the units that can no longer make it into combat make a Failed Charge Move.% 

\subsubsection*{Failed Charge Move}
If a unit does not roll a sufficient Charge Range, or is unable to complete the Charge for other reasons, it performs a Failed Charge Move instead. The highest \Dsix{} rolled when rolling Charge Range is the move distance. Wheel the Charging unit until it faces the centre of the Charged Unit or until it cannot Wheel anymore due to obstructions (whichever comes first). Then, move the Charging unit forward by the remaining amount of movement, if any is left. Once completed, the unit is no longer considered Charging.%

A Failed Charge Move may bring the unit within \distance{1} of other units and \impassableterrain{} as per \hyperref[section:Armies_UnitSpacing]{\textit{Unit Spacing}} rule.%

A unit that has Failed a Charge Move cannot move any further in this movement phase and cannot shoot in the following Shooting Phase, but rallied \wizards{} may cast spells in the Magic Phase.%

\end{multicols}

%\tikz[remember picture,overlay] \node[inner sep=0pt, above=3cm] at (current page.south) {\includegraphics[width=\textwidth]{art/ORNAMENT_048_avras.png}};
