\section{ARMIES}
\EWquote{Drop after constant drop, the Orcs became a green tide, and submerged our allies' Holds}
{Ninvael, emissary of the Pearl Throne}
\begin{multicols}{2}

As the player, it is your duty to command your army on the battlefield, but first you will have to pick one of the different factions available to you. If you are about to play your first game, we recommend that you play at least one game with a Premade Essence of War Patrol, that you can find in the Patrols section from page \pageref{Premade_Armies}. Each Patrol contains the following information:%

\subsubsection*{Troops \& Units}

Each Patrol has several entries that describe the types of forces available within that army. These are your warriors on the battlefield, and are represented by several models acting as a single element called a Unit.%

\subsubsection*{The Unit Entry}

Each unit has a unique entry as detailed on the Patrol pages, which detail how the unit behaves in the game. All unit entries share the same components:%

% this is used for adding images (ideally PNG or PDF); tweak that 0.35 parameter to fit to size; note that the image is centred
\begin{figure}[H]
\centering
\includegraphics[width=0.27\textwidth]{diagrams/FIGURE EW Unit Entry.png}
\caption{An example of Unit Entry from a Patrol.}
\label{fig:EW_Unit_Entry}
\end{figure}
% this is used for the caption; note \textbf{} for bold

\begin{itemize}[noitemsep,wide=0pt, leftmargin=\dimexpr\labelwidth + 2\labelsep\relax]
\item \textbf{Name, icon, formation and base size}: Each unit is identified by its name, its icon, and the number of models it is composed of, listed as files (width) x ranks (length). A crowned skull icon is used to represent unique personalities known as Characters, which are listed instead as a single model. The unit entry also notes the base size that is used by all the models in the unit.%
\item \textbf{Characteristics Profile}: Every unit has a characteristic profile that details the strengths and weaknesses of the soldiers. All models in the unit share this profile, described in the Characteristics section below.%
\item \textbf{Model Rules}: Describes additional rules that apply to the unit or the magical ability of a wizard. Not all units have Model Rules. Shooting units will have their shooting attacks described here as well. All the models in the unit share the same shooting attack rules.%
\item \textbf{Spells}: Most armies have access to powerful wielders of magic. These wizards and mages can cast the spells known to their army, which are noted with their unit entry. The full rules for magic and casting spells can be found in the Magic Phase Section.%
\end{itemize}

\subsection{Models}

\textit{The world of the The 9\textsuperscript{th} Age is home to many factions, and all have their own unique troops: like armoured swordsmen, savage orcs, brutal ogres, monstrous creatures, nimble archers and arcane wizards. In a Essence of War game, all these troops are commonly represented by miniature models.}%
\\

Models are placed on bases of a size and shape given in the unit's entry. All in-game measurements are made from this base, not from the model itself. Base sizes are given as two measurements in millimetres: front (width) \timess{} side (length).%

Two models are in base contact with each other if their bases are touching one another, including corner to corner contact.%

\subsection{Units}

%\textit{When operating as a trained army, most troops find strength in numbers and join together to fight in battle. Acting as a single unit, shooting, defending, and attacking at the same time. As a unit, each soldier can assist each other on the battlefield, multiplying their individual strength to be able to take on even the most fearsome opponents!}%

All models are part of a unit. A unit can be a group of models deployed in a formation consisting of ranks along the length of the unit, and files along the width of the unit. Such formed up models are called 'Rank and File models' (shortened to '\rnf{}'). Alternatively, a unit can consist of a single model operating on its own. Whenever a rule, ability, spell, etc. affects a unit, all models in the unit are affected.%

\subsubsection*{Unit Ranks and Formation}

The Unit Ranks of a unit is the number of ranks a unit has with at least one non-character model, except for single models which count as having no ranks (unless otherwise stated).%

When forming a unit, all models in the unit must have their bases aligned in base contact with each other and face the same direction. Units cannot be broken apart into smaller units. All ranks must always have the same width, except for the last rank which may be shorter than the other ranks due to \hyperref[section:GeneralPrinciples_RemovingCasualities]{\textit{Removing Casualties}} (see page \pageref{section:GeneralPrinciples_RemovingCasualities}).%

The width and formation a unit is deployed in is given in each unit's entry. For example: "6\timess{}3 models" means that the unit has 6 files and 3 ranks.%

\subsubsection*{Unit Boundary}\label{section:Armies_Unit_Boundary}

A Unit Boundary is an imaginary rectangle drawn around the outer edges of the unit. The Centre of a unit is the centre of its Unit Boundary. Two units are considered in base contact with each other if their Unit Boundaries are touching one another, including corner to corner contact. A unit can only move in base contact with an enemy unit through a successful Charge Move.%

%In any other circumstance, all units must end their movement at least \distance{1} away from \impassableterrain{} and from both friendly and enemy units.%

\subsubsection*{Unit Arcs and Unit Facing}

A unit has 4 Arcs and Facings: front, rear, and two flanks. Each arc is determined by extending a straight line from the corners of the unit's bases, in a 135 degrees angle from the unit's front (for the front arc), rear (for the rear arc) or flanks (for the flank arcs), see figure \ref{fig:EW_Arcs}. Models/Units are located in the Arc in which the centre of their Front is in.%

\begin{figure}[H]
\centering
\includegraphics[width=0.3\textwidth]{diagrams/FIGURE EW Arcs.png}
\caption{This unit has 3 ranks and 5 files. The Centre of the unit is the centre of the rectangle drawn around its outer edges (= Unit Boundary).}
\label{fig:EW_Arcs}
\end{figure}

\subsubsection*{Unit Spacing}\label{section:Armies_UnitSpacing}

All units must be separated from \impassableterrain{} and from other units by at least \distance{1} between their Unit Boundaries, with the following exceptions:%

\begin{itemize}[noitemsep,wide=0pt, leftmargin=\dimexpr\labelwidth + 2\labelsep\relax]
\item During an Advance Move, March Move, Flee Move, Pursuit and Overrun units are allowed within \distance{1} of these elements but need to end their move at least \distance{1} away from these elements.%
\item During a Failed Charge Move or a Successful Charge Move units are allowed to come within \distance{1} of these elements, including base contact with a unit that was the target of the Charge or with \impassableterrain{}.%
\end{itemize}
A unit may never move into base contact with an enemy unit it didn't declare a Charge against that turn.%

Once units are within \distance{1} of these elements due to any of the above exceptions they are allowed to remain there as long as they stay within \distance{1}. For example, a unit next to an \impassableterrain{} may Pivot and remain within \distance{1} of it.%

\subsection{Characteristics}

%\textit{Fast units get to pick their fights, nimble units strike before their enemy realises they're being attacked, strong units cleave through tough hide and \Armour{}, resilient units withstand blows which would fell lesser beings, while \Discipline{}d units stand and fight when others run. Most units excel in a specific task related to a characteristic while some are elite and excel at everything.}%

Models and Units have their attributes and abilities represented in the Characteristics profile. A higher value of a given Characteristic indicates that a model is more accomplished in that Characteristic except \aegis{} Saves, where a lower value is better. Usually each Characteristic is rated with a value between 0 and 10.%
\vfill\null
\columnbreak

Each unit entry contains the following Characteristics:%

% after the begintabular, the column structure of a table is specified as: {first-column-alignment-and-width type-of-separator-(here-it's-|) second-column-alignment-and-width}
% look at the Flux Cards table in the 08_magic_phase.tex file for a clearer example
\begin{tabular}{p{0.03\textwidth}|p{0.42\textwidth}}
\textbf{\AdvanceRateInitials{}} & \textbf{\AdvanceRate{}}: The distance the model can Advance Move in inches per turn.%
\\
% use & to separate between columns and \\ to go to next line
\textbf{\MarchRateInitials{}} & \textbf{\MarchRate{}}: The distance the model can March Move in inches per turn.%
\\
\textbf{\DisciplineInitials{}} & \textbf{\Discipline{}}: Represents the model's overall morale, bravery and a unit's ability to quickly reorganise during battle.%
\\
% use \hline to introduce a horizontal line
\hline
\textbf{\HealthPointsInitials{}} & \textbf{\HealthPoints{}}: When the model loses this many \HealthPoints{}, it is removed as a casualty.%
\\
\textbf{\DefensiveSkillInitials{}} & \textbf{\DefensiveSkill{}}: How well the model avoids being hit in melee. A high \DefensiveSkill{} may also represent models with equipment useful for parrying melee attacks, such as shields.%
\\
\textbf{\ResilienceInitials{}} & \textbf{\Resilience{}}: How easily the model withstands blows.%
\\
\textbf{\ArmourInitials{}} & \textbf{\Armour{}}: The \Armour{} of the model. This may represent particularly tough scaly hides as well as worn pieces of \Armour{}.%
\\
\textbf{\AegisInitials{}} & \textbf{\aegis{}}: A special protection of the model, the last defense after the model's \Armour{}. This may represent a model with magical defenses or regenerative abilities.%
\\
\hline
\textbf{\AttackValueInitials{}} & \textbf{\AttackValue{}}: The number of times the model can strike in melee. This maybe higher due to sheer attacking speed and skill, equipment such as dual wielded blades, or the size to potentially hit many opponents with a single blow.%
\\
\textbf{\OffensiveSkillInitials} & \textbf{\OffensiveSkill{}}: How good the model is at scoring hits in melee.%
\\
\textbf{\StrengthInitials{}} & \textbf{\Strength{}}: How easily the model can wound enemy models in melee. This may represent not just sheer physical strength, but also well-equipped models with weapons such as halberds or heavy two handed weaponry.%
\\
\textbf{\AP{}} & \textbf{\ArmourPenetration{}}: How well the model can penetrate the \Armour{} of enemy models in melee. This may be due to the physical attributes of the model, or also due to having weaponry designed to pierce \Armour{} such as lances or spears.%
\\
\textbf{\AgilityInitials{}} & \textbf{\Agility{}}: Models with higher \Agility{} strike first in melee. This may be influenced by the model's equipment. Models with an Aglity of zero for example represent models equipped with weapons that are particularly slow to strike with.%
\end{tabular}

Characteristic values may never be modified below 0, except \Agility{} and \AttackValue{{}} which may not be modified below 1. If a model has \Agility{} 0 in its unit entry, its \Agility{} value may not be modified. \Armour{} has a maximum value of 5 and \Agility{} and \Discipline{} have a maximum value of 10.%
\end{multicols}
