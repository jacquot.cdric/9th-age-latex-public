﻿
Compile with LuaLaTeX and NOT XeLaTeX or pdfLatex, else it can't work!
TeXLive strongly recommended, I had some trouble with MiKTeX.

Fonts need to be installed on your device before trying any compilation. They can be found in Layout/fonts.